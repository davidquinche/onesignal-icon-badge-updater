package com.catappult.OneSignalBadgeUpdater;


import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.NotificationCompat.WearableExtender;
import android.support.v4.app.RemoteInput;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.app.Activity;

import com.google.android.gms.gcm.GcmListenerService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;

@SuppressLint("NewApi")
public class GCMIntentService extends GcmListenerService implements Constants {

	private static final String LOG_TAG = "OneSignalBadgeUpdater_GCMIntentService";
	private static HashMap<Integer, ArrayList<String>> messageMap = new HashMap<Integer, ArrayList<String>>();

	@Override
	public void onMessageReceived(String from, Bundle extras) {
		
		
		Context applicationContext = getApplicationContext();
		
		if ( extras != null) {

			HashMap<String, String> badgeInstructions = extractIconBadgeInstructions(applicationContext, extras);
			
			String operation = badgeInstructions.get("badgeOperation");
			String count = badgeInstructions.get("badgeCount");
			if (operation == null || count == null){
				
			} else if (operation.equals(BADGE_OPERATION_INCREASE) ) {
				
				Integer badgeCount = Integer.parseInt(count);
				OneSignalBadgeUpdater.setBadge(OneSignalBadgeUpdater.getBadge(null, applicationContext) + badgeCount, null, applicationContext);
				
			} else if (operation.equals(BADGE_OPERATION_DECREASE) ) {
				
				
				Integer badgeCount = Integer.parseInt(count);
				OneSignalBadgeUpdater.setBadge( Math.max(0, OneSignalBadgeUpdater.getBadge(null, applicationContext) - badgeCount), null, applicationContext);
				
			} else if (operation.equals(BADGE_OPERATION_SET) ) {
				
				
				Integer badgeCount = Integer.parseInt(count);
				OneSignalBadgeUpdater.setBadge( badgeCount , null, applicationContext);
				
			} else if (operation.equals(BADGE_OPERATION_CLEAR) ) {
				
				
				OneSignalBadgeUpdater.setBadge( 0, null, applicationContext);
				
			}
			
		}
	}

	/*
	 * Parse bundle into normalized keys.
	 */
	private HashMap<String, String> extractIconBadgeInstructions(Context context, Bundle extras) {

		try {
			
			
			HashMap<String, String> instructions = new HashMap<String, String>();
			
			String processedBundle = extras.getString("custom");
			
			if (processedBundle != null) {
				JSONObject data = new JSONObject(processedBundle);
				processedBundle = data.getString("a");

				if (processedBundle != null) {
				
					data = new JSONObject(processedBundle);
					
					instructions.put("badgeCount", data.getString("badgeCount") == null ? "0" : data.getString("badgeCount"));
					instructions.put("badgeOperation", data.getString("badgeOperation") == null ? "" : data.getString("badgeOperation"));

				}
			}
			
			return instructions;
			
		} catch (Exception e) {
			
			return new HashMap<String, String>();

		}

	}



}
