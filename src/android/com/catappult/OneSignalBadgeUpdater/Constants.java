package com.catappult.OneSignalBadgeUpdater;

public interface Constants {
    public static final String COM_CATAPPULT_ONESIGNALBADGEUPDATER = "com.catappul.OneSignalBadgeUpdater";
    public static final String BADGE = "BADGE";
    public static final String BADGE_OPERATION_INCREASE = "INCREASE";
    public static final String BADGE_OPERATION_DECREASE = "DECREASE";
    public static final String BADGE_OPERATION_SET = "SET";
    public static final String BADGE_OPERATION_CLEAR = "CLEAR";
}
