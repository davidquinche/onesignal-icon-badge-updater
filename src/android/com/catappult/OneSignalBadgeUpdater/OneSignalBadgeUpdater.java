package com.catappult.OneSignalBadgeUpdater;

import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.iid.InstanceID;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;

import me.leolin.shortcutbadger.ShortcutBadger;

public class OneSignalBadgeUpdater extends CordovaPlugin implements Constants {

	public static final String LOG_TAG = "OneSignalBadgeUpdater";

	private static CordovaWebView gWebView;
	private static boolean gForeground = false;
	private static String registration_id = "";

	/**
	 * Gets the application context from cordova's main activity.
	 * 
	 * @return the application context
	 */
	private Context getApplicationContext() {
		return this.cordova.getActivity().getApplicationContext();
	}

	@Override
	public boolean execute(final String action, final JSONArray data, final CallbackContext callbackContext) {
		gWebView = this.webView;

		try {

			if (action.equalsIgnoreCase("getBadge")) {

				cordova.getThreadPool().execute(new Runnable() {
					public void run() {
						getBadge(callbackContext, getApplicationContext());
						
					}
				});
				
				return true;
			}

			if (action.equalsIgnoreCase("setBadge")) {

				cordova.getThreadPool().execute(new Runnable() {
					public void run() {

						int badge = data.optInt(0);
						setBadge( badge,callbackContext, getApplicationContext() );
						
					}
				});
				return true;
				
			}
			return false;
			
			// String name = "google_app_id";
			// String resource_text =
			// cordova.getActivity().getString(cordova.getActivity().getResources().getIdentifier(
			// name, "string", cordova.getActivity().getPackageName()));
			
			
		} catch (Exception e) {

			callbackContext.error(e.getMessage());
			return false;
		}

	}

	public static void setBadge( int badgeCount, CallbackContext callback,  Context context  ) {
		
		if (badgeCount > 0) {
			ShortcutBadger.applyCount(context, badgeCount);
		} else {
			ShortcutBadger.removeCount(context);
		}
		
		SharedPreferences.Editor editor = context.getSharedPreferences(BADGE, Context.MODE_PRIVATE).edit();

        editor.putInt(BADGE, badgeCount);
        editor.apply();
        
        if (callback != null){
        	
        	
        	PluginResult result = new PluginResult(PluginResult.Status.OK, badgeCount);
        	callback.sendPluginResult(result);
        }
		
		
	}

	public static Integer getBadge(CallbackContext callback, Context context) {
		
		try {
			
			SharedPreferences settings = context.getSharedPreferences(BADGE, Context.MODE_PRIVATE);
	        Integer badge = settings.getInt(BADGE, 0);
	        PluginResult result;
	        
	        
	        if (callback != null){
	        	
	        	result = new PluginResult(PluginResult.Status.OK, badge);
	        	callback.sendPluginResult(result);
	        	
	        }

	        return badge == null ? 0 : badge;
			
		} catch (Exception e){
			
			return 0;
			
		}
		 
	}
	
	@Override
	public void initialize(CordovaInterface cordova, CordovaWebView webView) {
		super.initialize(cordova, webView);
		gForeground = true;
	}

	@Override
	public void onPause(boolean multitasking) {
		super.onPause(multitasking);
		gForeground = false;

		SharedPreferences prefs = getApplicationContext().getSharedPreferences(COM_CATAPPULT_ONESIGNALBADGEUPDATER,
				Context.MODE_PRIVATE);
	}

	@Override
	public void onResume(boolean multitasking) {
		super.onResume(multitasking);
		gForeground = true;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		gForeground = false;
		gWebView = null;
	}


}
