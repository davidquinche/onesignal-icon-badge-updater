/* global cordova:false */
/* globals window */

/*!
 * Module dependencies.
 */

var exec = cordova.require('cordova/exec');


/*
 * ! Plugin
 */

module.exports = {
	/**
	 * Register for Push Notifications.
	 * 
	 * This method will instantiate a new copy of the PushNotification object
	 * and start the registration process.
	 * 
	 * @param {Object}
	 *            options
	 * @return {PushNotification} instance
	 */

	increase : function(count, callback, error) {

		this.get(function(badge) {
		this.set(badge + (count || 1), callback, error);
		}, this);

	},
	decrease : function(count, callback, error) {
		this.get(function(badge) {
			this.set(Math.max(0, badge - (count || 1)), callback, error);
		}, this);

	},
	
	set : function (badge, callback, error) {
	    var args = [parseInt(badge) || 0];

	            exec(callback, error, 'OneSignalBadgeUpdater','setBadge', args);
		
	},
	
	get : function (callback, error) {
		exec(callback, error, 'OneSignalBadgeUpdater','getBadge', null);
	},
	
};
